require(["jquery", "underscore"], function($, _) {
    $(function() {
        _.each($(".date"), function(d) {
            var $d = $(d);
            $d.text(new Date($d.text()).toString());
        });
    });
});
