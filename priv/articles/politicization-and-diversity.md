{"title": "Politicization and Diversity", "slug": "politicization-and-diversity", "author": "George Rogers", "date": "2017-06-19T10:26:00-06:00"}

<p>
The politicization of everything is highly harmful for the success of a cosmopolitan society.
Because political decision making collectivizes control of everything.
This pits groups against one another, that might not necessary be at odds with one another.
This encourages racial strife, class warfare, and other societal ills.
</p>
<p>
One of the reason Houston has less racial strife for a place with its demographics is its lack of a Zoning Code.
This means that land use is not politicized (which is the largest part of what gets politicized at the local level) which helps a good bit with racial tensions.
Because they did not want control of their neighborhoods to the city government that has very different racial and class makeup, therefore they did not support the Zoning Code during the three referenda.
</p>

<p>
<iframe width="560" height="315" src="https://www.youtube.com/embed/6S2NprJIw5g" frameborder="0" allowfullscreen></iframe>
</p>
