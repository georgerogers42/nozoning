{"title": "Homesteading, property, covenants and takings", "date": "2017-09-25T18:19:00-05:00", "slug": "chapter1", "author": "George Rogers"}

Homesteading is the means of creation of private property.
Before a piece of land becomes a piece of owned it must be claimed and used by some person or group.
Now after the piece of property becomes owned the claim of ownership and usership can be transfered by contract.
These contracts have three forms:

1. **The homesteader sells the land outright:** In this case the buyer has the same level of ownership as the homesteader had (outright ownership).
2. **The homesteader sells the land with a covenent attached:** In this case the buyer has a set of restrictions set by the homesteader, but still owns the land.
3. **The homesteader can rent out the land:** In this case the renter has a right to use the land with the restrictions set by the homesteader, and does not own the land.

## Regulatory taking
Let us take for example a person Alex that has owns a piece of land outright,
and he sells it to Barb with some set of conditions <em>c<sub>1</sub></em>.
Now a third party Malory comes around and forces some aditional set of conditions <em>c<sub>2</sub></em> that cannot be defended from <em>c<sub>1</sub></em> (and since Alex held the land outright there are no other conditions for Barb).
Malory is illegitimately taking (stealing) Barb's land, through regulation (Regulatory Taking).

Now she takes her case that Malory is taking her land, to a judge Houston;
who properly states that: 
"Malory is illegitimately taking (stealing) Barb's land, through regulation (Regulatory Taking)."
This immorality of Zoning leads to much corruption. 

Let us replace Judge Houston with another Judge Euclid who rules under a system corrupted by Zoning.
Judge Euclid rules in the favor of Malory because the State (City Hall) has the power to add (and remove) restrictions on land use (illegitimately justified under police power).

Mallory used the state to add the set of restrictions <em>c<sub>2</sub></em>, and because he is using "legal" means to do his dirty work Euclid rules in Malory's favor.

## Consequences of legalized regulatory takings (Zoning)
Zoning is the legalization of regulatory takings for the state.
Non state actors can manipulate the processes (especially in a democratic city/county/country) to their own ends. 
