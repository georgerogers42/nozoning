defmodule NoZoning.Mixfile do
  use Mix.Project

  def project do
    [
      app: :no_zoning,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {NoZoning, []},
      extra_applications: [:logger, :cowboy, :plug, :timex]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:cowboy, "~> 1.1.0"},
      {:poison, "~> 3.1.0"},
      {:earmark, "~> 1.2.4"},
      {:timex, "~> 3.1"},
      {:plug, "~> 1.0"}
    ]
  end
end
