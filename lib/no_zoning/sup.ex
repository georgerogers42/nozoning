defmodule NoZoning.Sup do
  use Supervisor

  def start_link(opts \\ []) do
    Supervisor.start_link(__MODULE__, [], opts)
  end

  def init([]) do
    children = [NoZoning.Articles]
    Supervisor.init(children, strategy: :one_for_one)
  end
end
