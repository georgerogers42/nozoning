defmodule NoZoning.Articles do
  defstruct article_list: [], article_map: %{}, page_map: %{}, max_pages: 0
  use GenServer
  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, [], [{:name, __MODULE__}|opts])
  end
  def init([]) do
    article_list = load_articles("#{Application.app_dir(:no_zoning)}/priv/articles/*.md")
    {:ok, %NoZoning.Articles{ article_list: article_list, article_map: article_map(article_list), page_map: page_map(article_list), max_pages: round(length(article_list) / page_length())-1}}
  end
  defp load_articles(pat) do
    Path.wildcard(pat) |> Stream.map(&NoZoning.Article.load_article/1) |> Enum.sort_by(&(&1.date), &Timex.after?(&1, &2))
  end
  defp article_map(article_list) do
    Stream.map(article_list, &{&1.slug, &1}) |> Enum.into(%{})
  end
  defp page_map(article_list) do
    Stream.with_index(article_list) |> Stream.map(&flip/1) |> Enum.into(%{})
  end
  defp flip({a, b}) do
    {b, a}
  end

  def handle_call({:get_article, name}, _from, state) do
    {:reply, Map.fetch(state.article_map, name), state}
  end
  def handle_call(:all_articles, _from, state) do
    {:reply, {:ok, state.article_list}, state}
  end
  def handle_call({:get_articles, a}, _from, state) do
    m = page_length()
    a = a*m
    {:reply, slice(state.page_map, a, min(a+m, Map.size(state.page_map))-1), state} 
  end
  def handle_call(:max_pages, _from, state) do
    {:reply, {:ok, state.max_pages}, state}
  end

  defp slice(m, s, e) do
    cond do
      s < Map.size(m) and e < Map.size(m) ->
        {:ok, Enum.map((s..e), &Map.fetch!(m, &1))}
    end
  end
  def get_articles(articles \\ __MODULE__, n) do
    GenServer.call(articles, {:get_articles, n})
  end
  def get_article(articles \\ __MODULE__, name) do
    GenServer.call(articles, {:get_article, name})
  end
  def all_articles(articles \\ __MODULE__) do
    GenServer.call(articles, :all_articles)
  end
  def max_pages(articles \\ __MODULE__) do
    GenServer.call(articles, :max_pages)
  end
  def page_length do
    4 
  end
end
