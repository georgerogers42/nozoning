defmodule NoZoning.Article do
  @derive [Poison.Encoder]
  defstruct slug: "", title: "", author: "", date: Timex.now, contents: ""
  def load_article(fname) do
    {header, body} = File.stream!(fname) |> Enum.split_while(&(&1 != "\n"))
    mdata = Poison.decode!(header, as: %NoZoning.Article{})
    {:ok, contents, _} = Earmark.as_html(to_string(body))
    %{ mdata | date: Timex.parse!(mdata.date, "{ISO:Extended}"), contents: contents}
  end
end
