defmodule NoZoning.Escape do
  def h(txt) do
    Plug.HTML.html_escape(txt)
  end
  def u(txt) do
    :http_uri.encode(txt)
  end
end
