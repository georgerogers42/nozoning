defmodule NoZoning.Web.Articles do
  require EEx
  import NoZoning.Escape

  def init(_type, req, state) do
    {:ok, req, state}
  end 
  def handle(req, state) do
    {n, req} = :cowboy_req.binding(:num, req)
    case parse_integer(n) do
      {:ok, n} ->
        case NoZoning.Articles.get_articles(n) do
          {:ok, articles} ->
            {:ok, m} = NoZoning.Articles.max_pages
            {:ok, req} = :cowboy_req.reply(200, [], template(articles, n, m), req)
            {:ok, req, state}
          {:error, _e} ->
            {:ok, req} = :cowboy_req.reply(404, [], "Page Not Found", req)
            {:ok, req, state}
        end
      {:error, _e} ->
        {:ok, req} = :cowboy_req.reply(404, [], "Page Not Found", req)
        {:ok, req, state}
    end
  end
  defp parse_integer(n) do
    try do
      {:ok, String.to_integer(n)}
    rescue
      e in ArgumentError ->
        {:error, e}
    end
  end
  def terminate(_reason, _req, _state) do
    :ok
  end

  EEx.function_from_file :def, :template, "templates/index.eex", [:articles, :n, :m]
end
