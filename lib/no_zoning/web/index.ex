defmodule NoZoning.Web.Index do
  require EEx
  import NoZoning.Escape

  def init(_type, req, state) do
    {:ok, req, state}
  end
  def handle(req, state) do
    {:ok, articles} = NoZoning.Articles.all_articles
    {:ok, m} = NoZoning.Articles.max_pages
    {:ok, req} = :cowboy_req.reply(200, [], template(articles, 0, m), req)
    {:ok, req, state}
  end
  def terminate(_reason, _req, _state) do
    :ok
  end

  EEx.function_from_file :def, :template, "templates/index.eex", [:articles, :n, :m]
end
