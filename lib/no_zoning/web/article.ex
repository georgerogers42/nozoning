defmodule NoZoning.Web.Article do
  require EEx
  import NoZoning.Escape

  def init(_type, req, state) do
    {:ok, req, state}
  end 
  def handle(req, state) do
    {slug, req} = :cowboy_req.binding(:slug, req)
    {:ok, article} = NoZoning.Articles.get_article(slug)
    {:ok, req} = :cowboy_req.reply(200, [], template(article), req)
    {:ok, req, state}
  end
  def terminate(_reason, _req, _state) do
    :ok
  end

  EEx.function_from_file :def, :template, "templates/article.eex", [:article]
end
