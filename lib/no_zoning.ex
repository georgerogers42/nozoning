defmodule NoZoning do
  use Application
  def start(_type, _args) do
    dispatch = :cowboy_router.compile(routes())
    p = port()
    {:ok, _} = :cowboy.start_http(:no_zoning, 100, [port: p], [env: [dispatch: dispatch]])
    :io.format("Started on port: ~b\n", [p])
    NoZoning.Sup.start_link
  end
  def port(default \\ "2000") do
    String.to_integer(System.get_env("PORT") || default)
  end
  def routes do
    [_: [{"/", NoZoning.Web.Index, %{}},
         {"/page/:num", NoZoning.Web.Articles, %{}},
         {"/article/:slug", NoZoning.Web.Article, %{}},
         {"/static/[...]", :cowboy_static, {:priv_dir, :no_zoning, "static/"}},
         {"/[...]", :cowboy_static, {:priv_dir, :no_zoning, "static/"}}]]
  end
end
